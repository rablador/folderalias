*This project is no longer actively maintained or supported! I simply don't have the time or energy for it anymore (and haven't had for a while). The code is free to use and fork into other projects, so please don't hesitate to create something awesome with it if you like :).*
# - - -

# FolderAlias

Trouble navigating your project folders? Side bar a mess? FolderAlias lets you rename your project folders by creating aliases for them!

All OS:s supported!

Since the API for Sublime Text 2 is lacking some necessary features FolderAlias only runs on Sublime Text 3.

Installation
------------

**Package Control**  
Easiest and quickest way to install is through Package Control:  
[http://wbond.net/sublime\_packages/package\_control](http://wbond.net/sublime\_packages/package\_control)

Overview
--------

FolderAlias lets you rename your project folders by creating aliases for them. You can also create dummy placeholders to divide your project folders as you please. No settings required, simply right-click on a folder in the side bar and set whatever name you like. Ideal for projects setups that are not easily configured!

Usage
-----

1. Right-click on a folder.
2. Choose an action.
3. Optionally input a folder alias or a number of dummies.
4. Immediate result!

Certain actions can be found in the main menu (Project) or in the command palette as well.

Features
--------

#### Set alias
Sets or updates a new alias for the selected folder.

#### Remove alias
Removes alias, reverting back to the original folder name. Dummy folders are removed.

#### Remove all aliases
Removes all aliases only. Dummy folders stay untouched.

#### Remove all dummies
Removes all dummies.

#### Remove all aliases and dummies
Removes all aliases and dummies.

#### Create dummy
Creates a dummy folder with no proper location or content.

#### Create multiple dummies
Creates multiple dummy folders with no proper location or content.

#### Clone to dummy
Clones a folder name/alias to a dummy folder with no proper location or content.

#### Clone to multiple dummies
Clones a folder name/alias to multiple dummy folders with no proper location or content.

Feedback
--------

Feedback on FolderAlias is highly appreciated. Please direct all your feedback, questions, suggestions etc to the issue tracker here:
[https://bitbucket.org/rablador/folderalias/issues](https://bitbucket.org/rablador/folderalias/issues)

About
-----

FolderAlias is created and maintained by Rablador. The code is free.