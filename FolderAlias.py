from random import randint
from os import path
import sublime, sublime_plugin

class FolderAliasCommand(sublime_plugin.WindowCommand):
  """
  Window command class for Sublime Text.
  """

  def run(self, **kwargs):
    """
    Sublime Text main method.
    """
    # Running Sublime Text 2?
    self.is_st2 = int(sublime.version()) < 3000
    if self.is_st2:
      sublime.error_message('FolderAlias says: "I am very sorry, but since the API for Sublime Text 2 is lacking some necessary features I only run on Sublime Text 3. You will have to uninstall and forget all about me."')
      return

    # Set a window reference.
    self.window = sublime.active_window()

    # Get path of folder to create alias for.
    self.folder_path = ''
    if 'paths' in kwargs:
      self.folder_path = kwargs['paths'][0]
    # Set run mode, "set" or "remove".
    self.run_mode = kwargs['mode']

    # Get project data, either from current window or loaded project file.
    project_data = self.window.project_data()
    # Set folder anme to last item in path.
    folder_name = path.basename(self.folder_path)

    # @todo A bit contrived. To be refactored.
    # Do certain actions depending on run mode, such as marking dummies for removal or
    # defining/removing folder name.
    dummies_to_remove = []
    for i, folder in enumerate(project_data['folders']):
      is_dummy = 'FolderAlias_' == folder['path'][:12]

      # Remove all aliases.
      if ('remove_aliases' == self.run_mode) and not is_dummy:
        if 'name' in folder:
          del folder['name']
      # Remove all dummies.
      elif ('remove_dummies' == self.run_mode) and is_dummy:
        dummies_to_remove.append(folder['path'])
      # Remove all aliases and dummies.
      elif 'remove_all' == self.run_mode:
        if not is_dummy:
          if 'name' in folder:
            del folder['name']
        else:
          dummies_to_remove.append(folder['path'])

      # Define dummy name.
      elif ('clone' == self.run_mode[:5]) and is_dummy:
        if folder_name == path.basename(folder['path']):
          folder_name = folder['name']
          break

      # Define or remove alias.
      elif self.folder_path == folder['path']:
        if 'name' in folder:
          # Remove or prepare new folder alias depending on run mode.
          if 'remove' == self.run_mode:
            if not is_dummy:
              del folder['name']
            else:
              dummies_to_remove.append(folder['path'])
            break
          else:
            folder_name = folder['name']
            break

    # Create new project data object by removing dummies marked for removal.
    self.project_data = {'folders': []}
    for i, folder in enumerate(project_data['folders']):
      if not folder['path'] in dummies_to_remove:
        self.project_data['folders'].append(folder)

    # Reload project folders.
    if 'remove' == self.run_mode[:6]:
      self.window.set_project_data(self.project_data)
    # Go directly to alias creation.
    elif 'clone' == self.run_mode[:5]:
      self.on_done_set(folder_name)
    # Show input panel to set alias.
    else:
      self.window.show_input_panel('Set folder alias:', folder_name, self.on_done_set, self.on_change, self.on_cancel)

  def on_done_set(self, folder_name):
    """
    Callback to input panel. Sets new folder alias or creates dummy.
    """
    # Show input panel to create a certain number of aliases.
    if ('dummy_multiple' == self.run_mode) or ('clone_multiple' == self.run_mode):
      self.dummy_name = folder_name
      self.window.show_input_panel('Set number of dummies:', '', self.on_done_set_x, self.on_change, self.on_cancel)
    # Create dummy folder.
    if ('dummy' == self.run_mode) or ('clone' == self.run_mode):
      dummy_folder_path = 'FolderAlias_' + str(randint(1000000, 9000000))
      dummy_folder = {'path': dummy_folder_path, 'name': folder_name}
      self.project_data['folders'].append(dummy_folder)
    # Set folder alias.
    else:
      for i, folder in enumerate(self.project_data['folders']):
        if self.folder_path == folder['path']:
          self.project_data['folders'][i]['name'] = folder_name

    self.window.set_project_data(self.project_data)

  def on_done_set_x(self, number_of_dummies):
    """
    Callback to input panel. Creates multiple dummies.
    """
    # Create multiple dummy folders.
    for i in range(0, int(number_of_dummies)):
      dummy_folder_path = 'FolderAlias_' + str(randint(1000000, 9000000))
      dummy_folder = {'path': dummy_folder_path, 'name': self.dummy_name}
      self.project_data['folders'].append(dummy_folder)

    self.window.set_project_data(self.project_data)

  def on_change(self, folder_name_input):
    """
    Callback to input panel.
    """
    return

  def on_cancel(self):
    """
    Callback to input panel.
    """
    return